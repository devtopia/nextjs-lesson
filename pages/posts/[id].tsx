import { GetStaticPaths, GetStaticProps } from "next";
import Link from "next/link";

import Layout from "../../components/Layout";
import BackIcon from "../../components/icons/back-icon";
import { getPostData } from "../../lib/posts";
import { getAllPostIds } from "../../lib/posts";
import { PostType } from "../../types";

interface Props {
  post: PostType;
}

const Post: React.FC<Props> = (props) => {
  const { post } = props;
  return (
    <Layout title={post.title}>
      <p className="m-4">
        {"ID : "}
        {post.id}
      </p>
      <p className="mb-8 text-xl font-bold">{post.title}</p>
      <p className="px-10">{post.body}</p>
      <Link href="/blog-page">
        <div className="flex cursor-pointer mt-12">
          <BackIcon />
          <span>Back to blog-page</span>
        </div>
      </Link>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const post = await getPostData(params?.id as string);

  return {
    props: { post },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getAllPostIds();

  return {
    paths,
    fallback: false,
  };
};

export default Post;
