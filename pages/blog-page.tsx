import { GetStaticProps } from "next";

import Layout from "../components/Layout";
import Post from "../components/Post";
import { getAllPostData } from "../lib/posts";
import { PostType } from "../types";

interface Props {
  posts: PostType[];
}

const Blog: React.FC<Props> = (props) => {
  const { posts } = props;

  return (
    <Layout title="Blog">
      <ul className="m-10">
        {posts && posts.map((post) => <Post key={post.id} post={post} />)}
      </ul>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const posts = await getAllPostData();
  return {
    props: { posts },
  };
};

export default Blog;
