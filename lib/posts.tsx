// https://developer.mozilla.org/ja/docs/Web/API/Fetch_API/Using_Fetch
import { PostType } from "../types";

const url = "https://jsonplaceholder.typicode.com/posts";

export const getAllPostData = async () => {
  const posts = await fetch(url).then((response) => response.json());
  return posts;
};

export const getAllPostIds = async () => {
  const posts = await getAllPostData();

  return posts.map((post: PostType) => {
    return {
      params: {
        id: String(post.id),
      },
    };
  });
};

export const getPostData = async (id: string) => {
  const post = await fetch(`${url}/${id}`).then((response) => response.json());
  return post;
};
